#!/bin/bash
#--------------------------------------------------------------------------------
# Starts the container based on Ubuntu 20.04 and detaches from it. The container
#  is named 'tex_cont'.
#--------------------------------------------------------------------------------

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

echo "--------------------------------------------"
docker images
echo "--------------------------------------------"
docker container ls
echo "--------------------------------------------"
docker ps -a
echo "--------------------------------------------"

# Back to the original location
popd > /dev/null
