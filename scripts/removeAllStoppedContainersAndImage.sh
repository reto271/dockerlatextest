#!/bin/bash
#--------------------------------------------------------------------------------
# Removes ALL stopped containers. Caution!
# Removs all stopped containers and cleans the image named latex_container
#--------------------------------------------------------------------------------

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

docker system prune
feedback=$?

if [ 0 -eq ${feedback} ] ; then
    docker rmi reto271/latex_container
    feedback=$?
fi

# Back to the original location
popd > /dev/null

exit ${feedback}
