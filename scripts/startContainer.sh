#!/bin/bash
#--------------------------------------------------------------------------------
# Starts the container based on Ubuntu 20.04 and detaches from it. The container
#  is named 'tex_cont'.
#--------------------------------------------------------------------------------

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

id=$(docker ps -a | grep tex_cont | awk '{print $1}' )
if [ "" == "${id}" ] ; then
    echo "container does not exist -> create it"
    docker run -d --name tex_cont -it -v ~/git:/git reto271/latex_container
else
    docker start ${id}
fi

# Back to the original location
popd > /dev/null
