#!/bin/bash
#-----------------------------------------------
# Attaches to the running container named tex_cont.
#-----------------------------------------------

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

./scripts/startContainer.sh
docker attach tex_cont

feedback=$?

if [ 0 -ne ${feedback} ] ; then
    echo ""
    echo "Is the container prepared, e.g. 'createImage.sh' executed?"
    echo "Is the container started, e.g. 'startContainer.sh'executed?"
fi

# Back to the original location
popd > /dev/null
