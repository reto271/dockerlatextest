#!/bin/bash
#-----------------------------------------------
# Creates the container based on Ubuntu 20.04
#-----------------------------------------------

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

docker pull ubuntu:20.04
feedback=$?

if [ 0 -eq ${feedback} ] ; then
    docker build --tag reto271/latex_container .
    feedback=$?
fi


# Back to the original location
popd > /dev/null

exit ${feedback}
