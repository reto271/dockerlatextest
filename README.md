# Compile a Latex Document in a Docker

The goal of the project is to test:
1. Create a docker container
1. Configure it to run latex
1. Setup a small latex project
1. Compile the document within the container
   1. Manually (by login)
   1. From outside of the container (by script)
1. Use the container to run CI on Jenkins (on Raspberry Pi)
1. Use the container to run CI/CD in gitlab


## Create Container
* Container based on Ubuntu 20.04 created
* Scripts to create and remove it.
* Container named: latex_container
* Latex tools not yet installed
* Mounts ~/git to /git within the container. _Make all my git projects in ~/git/ available in the container._
* Scripts to start/attach to the container

## Setup Docker Account
1. Create an account at https://hub.docker.com/
1. Create a project, e.g. latex_container
1. Login to the docker account on the command line (docker login)
   * The docker credentials will be stored in a plain file on the disk.
   * Eventually setup 'pass' as key store. Instruction see #[Setup key store](#setup-pass-key-store)

## Push Image to dockerhub
1. Login to your docker account: docker login
1. Push the image: docker push <your docker id>/latex_container

## Setup pass Key Store
This section is only necessary to store your docker password safe. Without the key-store the password will be stored in a plain file in you home directory.

Install and setup a key
~~~ bash
sudo apt install pass
gpg --full-generate-key # Create public-private key
pass init <public key> # <public-key>, namely your e-mail address
~~~

Setup the connection
~~~ bash
mkdir -p Tools/docker-credential
cd Tools/docker-credential
wget https://github.com/docker/docker-credential-helpers/releases/download/v0.6.3/docker-credential-pass-v0.6.3-amd64.tar.gz
tar xvzf docker-credential-pass-v0.6.3-amd64.tar.gz
chmod a+x docker-credential-pass
mkdir ~/.docker
echo '{ "credsStore": "pass" }' > ~/.docker/config.json
# Alternative to the sym-link, add to Tools/docker-credential to $PATH
sudo ln -s ${HOME}/Tools/docker-credential/docker-credential-pass docker-credential-pass
# Set the password to: pass is initialized
docker login # Which will now store credentials in Pass
~~~
