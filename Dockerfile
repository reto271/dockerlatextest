# Use ubuntu 20.04 as base
FROM ubuntu:20.04

# Add requird tools
RUN apt-get update
# Do not install the full, it takes too long!!! RUN apt-get -y install texlive-full
# Install a 'decent' selection.
RUN DEBIAN_FRONTEND='noninteractive' apt-get -y install texlive texlive-lang-german texlive-science texlive-latex-extra
RUN DEBIAN_FRONTEND='noninteractive' apt-get -y install git
RUN DEBIAN_FRONTEND='noninteractive' apt-get -y install make

# RUN mkdir "temp"
# COPY . temp
#
# cmd "cd temp"
# qcmd "/build.sh"
